const express = require('express'),
resultCtrl = require('../controller/result.controller'),
router = express.Router()

router.post('/getresult',resultCtrl.getresult);
router.post('/getslip',resultCtrl.getslip);
router.post('/getname',resultCtrl.getname);
router.get('/lecturer', resultCtrl.lecturer);
router.post('/get_student', resultCtrl.get_student);
router.post('/get_marks', resultCtrl.get_marks);
router.post('/update_mark', resultCtrl.update_mark);


module.exports = router;
