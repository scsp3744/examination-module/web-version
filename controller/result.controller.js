const mongoose = require('mongoose'),
Student = mongoose.model('Student'),
Result = mongoose.model('Result'),
Course = mongoose.model('Course'),
Mark = mongoose.model('Mark'),
PDFDocument = require('pdfkit'),
fs = require('fs')

const getresult = function(req, res, next){

    Matric_No = req.body.matric_no
    Sem = req.body.sem
    console.log("CTRL: ",Matric_No, Sem);
    Student.findOne({matric : Matric_No})
    .exec()
    .then((result) => {
      if(!result)
      {
        console.log("not found");
        return res.render('home',{error:"Invalid Matric Number",student: "em" , r : "em"});
      }
      else
      { 
          var result_length = result._doc.Result_ID.length
          console.log("Student result length: ",result._doc.Result_ID.length);
          console.log("Sem: ",result._doc.Result_ID.Sem);
          
            for(i=0 ; i<result_length; i++)
              {
                console.log("Item: ",result._doc.Result_ID[i].Id,result._doc.Result_ID[i].Sem)
                var R_Sem = result._doc.Result_ID[i].Sem;
                var R_Id =  result._doc.Result_ID[i].Id;
                if(R_Sem == Sem )
                {
                  console.log("Found");
                  Result.findOne({"_id":Object(R_Id)})
                  .exec()
                  .then((doc) => {
                    if(!doc){
                      console.log("NO RECORD");
                      return res.render('home',{error:"No Record",student: "em" , r : "em"});
                    }
                    else
                    {
                      console.log("Found");
                      var cs = doc.Course;
                      return res.render('home',{ error:"em",student: result , r : cs, r_id:R_Id});
                    }
                  })
                }
                else
                {
                  console.log("NO RECORD");
                  return res.render('home',{error:"No Record",student: "em" , r : "em"});
                }
              }
      }

    })
}

const getslip = function(req,res,next){
  
  stuid = req.body.Student;
  rid = req.body.Result;
  console.log(stuid, rid);
  Student.findOne({_id : stuid})
    .exec()
    .then((result) => {
      if(!result)
      {
        console.log("not found");
        return res.render('home',{error:"Invalid",student: "em" , r : "em"});
      }
      else
      { 
        var result_length = result._doc.Result_ID.length
        var R_Sem = [];
        for(i=0 ; i<result_length; i++)
        {
          if (result._doc.Result_ID[i].Id==rid)
          {
            var s = result._doc.Result_ID[i].Sem;
            var ses = s.substring(0,s.indexOf('-'));
            var sem = s.substring((s.indexOf('-')+1));
            R_Sem.ses=ses;
            R_Sem.sem=sem;
          }
        }
        console.log("Found");
        Result.findOne({_id : rid})
        .exec()
        .then((doc) => {
          if(!doc){
            console.log("NO RECORD");
            return res.render('home',{error:"No Record",student: "em" , r : "em"});
          }
          else
          {
            console.log("Found");
            var cs = doc.Course;
            var ch = 0;
            for(var x = 0 ; x < cs.length ; x++ )
            {      
              ch += cs[x].Credit_hour;
            }
            R_Sem.ch = ch;
            var png = 0;
            for(var x = 0 ; x < cs.length ; x++ )
            {
              png += cs[x].Pointer *  (cs[x].Credit_hour / ch);
            } 
            R_Sem.png = png;
            return res.render('slip',{student: result , r : cs, sem : R_Sem});
          }
          })
      }

  })
  
}

const getname = function(req,res,next){

  Sem = req.body.sem
  console.log(Sem);
  Student.find({'Result_ID.Sem' : Sem},{name:1,Result_ID:{$elemMatch:{Sem:req.body.sem}}})
  .then((result) => {
    if(!result)
    {
      console.log("not found");
      
    }
      else{
      console.log(result);
      return res.render('staff',{student: result});
      }
  
  });
}

const lecturer = function (req, res, next) {

    Course.find(function (err, result) {
      if (!result) {
        console.log(err);
      } else {
        //   console.log(result);
        for (i = 0; i < result[0].length; i++) {
          console.log(result[i].Code);
        }
      }
      return res.render('lecturer',{ list:"em", course_list: result });
    });
  };

const get_student = function (req, res, next) {
    
  var courseId = req.body.id;
    console.log(courseId);
    var course=[];
    Course.find(function (err, result) {
      course=result;
    });
    Course.findOne({ _id: courseId })
      .populate("Student.StudentId")
      .populate("Student.MarkId")
      .exec()
      .then((doc) => {
        if (doc) {
          console.log(doc);
          return res.render('lecturer',{ list: doc,course_list: course });
        } else {
          console.log(err);
        }
      });
  };
  
  const get_marks = function (req, res, next) {
    
    var student=[];
    var mark_id = req.body.mark;
    student.name = req.body.name;
    student.matric = req.body.matric;
  
    Mark.findById({ _id: mark_id }).then((result) => {
      if (!result) {
        console.log(err);
      } else {
        return res.render('update',{ mark: result , student:student });
      }
    });
  };
  
  const update_mark = function (req, res) {
    console.log("Update here");
    console.log(req.body);
    console.log(req.body._id);
  
    const id = req.body._id;
    Mark.findById({ _id: id })
        .exec()
        .then(r => {
            if (r) {
              Mark.updateOne({ _id: id }, { $set: {
                Assignment: req.body.Assignment ,
                Quiz: req.body.Quiz ,
                MidTerm: req.body.MidTerm ,
                Project: req.body.Project ,
                Presentation: req.body.Presentation ,
                Final: req.body.Final 
                } }).exec();
                
                  return res.redirect('/lecturer');
            } 
            else {
              console.log(err);
            }
        });
};


module.exports = {
    getresult,
    getslip,
    getname,
    get_student,
    lecturer,
    get_marks,
    update_mark
}