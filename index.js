require("./config/config")
require("./models/db")

// Set up
var express  = require('express');
var app = express();                               // create our app w/ express
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var cors = require('cors');
var routes = require('./routes/student.routes');

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());
app.use(cors());
app.use('/', routes);
app.set('view engine','ejs');
app.use( express.static( "public" ) );
app.get('/',(req,res)=>{
    res.render('home',{error:"em",student: "em" , r : "em"});
});
app.get('/staff',(req,res)=>{
    res.render('staff',{student: "em" });
});
// listen (start app with node server.js) ======================================
app.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));